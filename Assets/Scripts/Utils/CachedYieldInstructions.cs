using UnityEngine;

namespace Engine.Utils
{
    public static class CachedYieldInstructions
    {
        public static readonly YieldInstruction WaitForFixedUpdate = new WaitForFixedUpdate();
        public static readonly YieldInstruction NullableWait = null;
    }
}