﻿using System;
using UnityEngine;

namespace Engine.Utils.Prefs
{
    public static class PlayerPrefsUtils
    {
        public static void SaveString(string prefsKey, string prefsValue)
        {
            PlayerPrefs.SetString(prefsKey, prefsValue);
            SavePlayerPrefsToDisk();
        }

        public static string GetString(string prefsKey, string defaultPrefsValue = "")
        {
            return PlayerPrefs.GetString(prefsKey, defaultPrefsValue);
        }

        public static void SaveInt(string prefsKey, int prefsValue)
        {
            PlayerPrefs.SetInt(prefsKey, prefsValue);
            SavePlayerPrefsToDisk();
        }

        public static int GetInt(string prefsKey, int defaultPrefsValue = 0)
        {
            return PlayerPrefs.GetInt(prefsKey, defaultPrefsValue);
        }

        public static void SaveBool(string prefsKey, bool prefsValue)
        {
            SaveInt(prefsKey, Convert.ToInt32(prefsValue));
        }

        public static bool GetBool(string prefsKey, bool defaultPrefsValue = false)
        {
            var value = Convert.ToBoolean(GetInt(prefsKey, Convert.ToInt32(defaultPrefsValue)));
            return value;
        }

        private static void SavePlayerPrefsToDisk()
        {
            PlayerPrefs.Save();
        }

        public static void DeleteAll()
        {
            PlayerPrefs.DeleteAll();
            SavePlayerPrefsToDisk();
        }
    }
}