﻿using System.Collections;
using System.IO;
using System.Text;
using Engine.Utils.Extensions;
using JetBrains.Annotations;
using UniRx.Async;
using UnityEngine;

namespace Engine.Utils
{
    public static class ObjectLoader
    {
        public static async UniTask<T> LoadObjectFromDiskAsync<T>(string pathToFile)
        {            
            if (!File.Exists(pathToFile))
            {
                return default(T);
            }

            var textBuffer = await FileUtils.ReadAllTextAsync(pathToFile);

            return JsonUtility.FromJson<T>(textBuffer);
        }

        public static async UniTask<bool> SaveObjectToDiskAsync<T>(T objectToSave, string pathToFile)
        {
            var saved = false;

            if (objectToSave != null)
            {
                saved = true;
                var stringToSave = JsonUtility.ToJson(objectToSave);
                await FileUtils.WriteAllTextAsync(pathToFile, stringToSave);
            }

            return saved;
        }        
    }
}