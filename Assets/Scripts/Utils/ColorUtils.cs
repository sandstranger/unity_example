﻿using UnityEngine;

namespace Engine.Utils
{
    public static class ColorUtils 
    {
        public static Color RandomColor
        {
            get
            {
                const float maxColorValue = 1.0f;

                float GetRandomColorValue()
                {
                    return UnityEngine.Random.Range(0, maxColorValue);
                }

                return new Color(GetRandomColorValue(), GetRandomColorValue(), GetRandomColorValue(), maxColorValue);
            }
        }        
    }
}
