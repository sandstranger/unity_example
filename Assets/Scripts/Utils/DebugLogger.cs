using System;
using UnityEngine;

namespace Engine.Utils
{
    public static class DebugLogger
    {
        private static bool ShowLog => Application.isEditor || Debug.isDebugBuild;

        public static void Log(string message)
        {
            if (!ShowLog) return;
            
            Debug.Log(message);
        }

        public static void LogException(Exception e)
        {
            if (!ShowLog) return;

            Debug.LogException(e);
        }
    }
}