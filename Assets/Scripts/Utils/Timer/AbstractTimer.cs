﻿using System;
using UniRx;
using UnityEngine;

namespace Engine.Utils.Timer
{
    public abstract class AbstractTimer<T> : ITimer<T>
    {
        public IObservable<T> TimeChangedObservable => _timeValueProperty;

        public T TimerValue
        {
            get => _timeValueProperty.Value;
            set => _timeValueProperty.Value = value;
        }

        private bool _startTimer = false;

        public virtual bool StartTimer
        {
            get => _startTimer;
            set => _startTimer = value;
        }

        private const long IntervalPeriod = 1;

        private readonly IDisposable _timerSubscription;
        private readonly IObservable<long> _timerObserver;
        private readonly ReactiveProperty<T> _timeValueProperty = new ReactiveProperty<T>(default(T));

        private protected AbstractTimer()
        {
            _timerObserver = Observable.Interval(TimeSpan.FromSeconds(IntervalPeriod))
                .Where(ticker => StartTimer).Share();

            _timerSubscription = _timerObserver.Subscribe(TimerJob);
        }

        private protected abstract void TimerJob(long seconds);

        public virtual void ResetTimer()
        {
            TimerValue = default(T);
        }

        public virtual void Dispose()
        {
            _timeValueProperty.Dispose();
            _timerSubscription.Dispose();
        }
    }
}