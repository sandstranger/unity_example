using System;

namespace Engine.Utils.Timer
{
    public interface ITimer<T> : IDisposable
    {
        IObservable<T> TimeChangedObservable { get; }
        T TimerValue { get; set; }
        bool StartTimer { get; set; }
        void ResetTimer();
        string ToString();
    }
}