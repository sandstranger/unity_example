﻿using System;

namespace Engine.Utils.Timer
{
    public sealed class GameDateTimer : AbstractTimer<DateTime>
    {
        public static GameDateTimer NewInstance => new GameDateTimer();

        private GameDateTimer() : base()
        {
            ResetTimer();
        }

        private protected override void TimerJob(long seconds)
        {
            if (!StartTimer) return;

            TimerValue = TimerValue.AddDays(IntervalPeriod);
        }

        public override string ToString()
        {
            return TimerValue.ToString(GameDateTimerUtils.GameDateFormat);
        }

        public override void ResetTimer()
        {
            DateTime.TryParse(GameDateTimerUtils.StartGameDateString, out var dateTime);
            TimerValue = dateTime;
        }
    }

    public static class GameDateTimerUtils
    {
        public const string GameDateFormat = "dd.MM.yyyy";
        public const string StartGameDateString = "15.04.2004";
        public static DateTime StartGameDate => StartGameDateLazy.Value;

        private static readonly Lazy<DateTime> StartGameDateLazy =
            new Lazy<DateTime>(() => ParseStringToDate(StartGameDateString));

        public static DateTime ParseStringToDate(string targetDateString)
        {
            DateTime.TryParse(targetDateString, out var newDate);
            return newDate;
        }
    }
}