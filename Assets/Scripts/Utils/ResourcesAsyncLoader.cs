﻿using System.Collections;
using Engine.Utils.Extensions;
using UniRx;
using UniRx.Async;
using UnityEngine;

namespace Engine.Utils
{
    public static class ResourcesAsyncLoader
    {
        public static async UniTask<T> LoadAsync<T>(string pathToPrefab) where T : Object
        {
            var request = await Resources.LoadAsync<T>(pathToPrefab);

            return request as T;
        }
    }

    public sealed class ResourcesAsyncLoader<T> where T : Object
    {
        public bool IsDone { get; private set; }
        public YieldInstruction TaskYieldOperation { get; }
        public T Result { get; private set; }
        private readonly string _pathToResource;

        public ResourcesAsyncLoader(string pathToResource)
        {
            TaskYieldOperation = Application.isPlaying
                ? CachedYieldInstructions.WaitForFixedUpdate
                : CachedYieldInstructions.NullableWait;

            _pathToResource = string.Intern(pathToResource);

            MainThreadDispatcher.StartCoroutine(LoadAsync());
        }

        private IEnumerator LoadAsync()
        {
            var request = Resources.LoadAsync<T>(_pathToResource);
            do
            {
                yield return TaskYieldOperation;
            } while (!request.isDone);

            Result = request.asset as T;
            IsDone = true;
        }
    }
}