using UnityEngine;

namespace Engine.Utils.Extensions
{
    public static class ColorExtensions
    {
        public static string ToHtmlStringRGBA(this Color color)
        {
            return $"#{ColorUtility.ToHtmlStringRGBA(color)}";
        }
    }
}