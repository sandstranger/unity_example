
namespace Engine.Utils.Extensions
{
    public static class NullableExtensions
    {
        public static bool Null<T>(this T value) where T : class
        {
            return value == null;
        }
    }
}