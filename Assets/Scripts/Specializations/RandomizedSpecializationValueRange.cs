using System;
using UnityEngine;

namespace Engine.Model.Office.Specializations
{
    [Serializable]
    internal struct RandomizedSpecializationValueRange
    {
        internal uint Value
        {
            get
            {
                var updateCachedValue =  !_cachedValue.HasValue;

                if (updateCachedValue)
                {
                    ResetCachedValue();
                }

                return _cachedValue.Value;
            }
        }

        [NonSerialized]
        private uint? _cachedValue;
        [NonSerialized]
        private bool _resetCachedRange;

        [SerializeField] private Range _range;

        internal void ResetCachedValue()
        {
            _cachedValue = GetRandomFromRange();
        }
        
        private uint GetRandomFromRange()
        {
            const int defaultRange = 0;
            var outOfRangeValues = _range.MinRange < defaultRange || _range.MaxRange < defaultRange ||
                                   (_range.MinRange > defaultRange && _range.MaxRange <= _range.MinRange);
            if (outOfRangeValues)
            {
                throw new ArgumentOutOfRangeException(
                    $"Range Values, min range ={_range.MinRange}, max range={_range.MaxRange} out of range");
            }

            if (_range.MaxRange == defaultRange)
            {
                return defaultRange;
            }

            return Convert.ToUInt32(UnityEngine.Random.Range(_range.MinRange,
                _range.MaxRange + 1));
        }
    }
}