namespace Engine.Model.Office.Specializations
{
    public enum SpecializationType
    {
        Designer,
        Programmer,
        Advertiser,
        Tester,
        Marketer,
        ArtDirector,
        TechnicalDirector,
        SalesMan
    }
}