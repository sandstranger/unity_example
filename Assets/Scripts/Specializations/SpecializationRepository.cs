using System;
using System.IO;
using JetBrains.Annotations;
using RotaryHeart.Lib.SerializableDictionary;
using UnityEngine;

namespace Engine.Model.Office.Specializations
{
    public interface ISpecializationRepository
    {
        [CanBeNull]
        ISpecialization GetSpecialization(SpecializationType specializationType);
    }

    [CreateAssetMenu(fileName = "SpecializationsRepository", menuName = "Game/Specialization Repository", order = 1)]
    public sealed class SpecializationRepository : ScriptableObject, ISpecializationRepository
    {
        [SerializeField] private SpecializationDictionary _specializations;

        public ISpecialization GetSpecialization(SpecializationType specializationType)
        {
            _specializations.TryGetValue(specializationType, out var specialization);

            specialization?.ResetCachedValues();

            return specialization;
        }
    }

    [System.Serializable]
    internal sealed class
        SpecializationDictionary : SerializableDictionaryBase<SpecializationType, RandomizedSpecialization>
    {
    };

    public static class SpecializationRepositoryUtils
    {
        public static ISpecializationRepository SpecializationRepository => SpecializationsRepository.Value;

        private static readonly string PathToSpecializationRepository =
            Path.Combine("Configs", "Specializations", "SpecializationsRepository");

        private static readonly Lazy<SpecializationRepository> SpecializationsRepository
            = new Lazy<SpecializationRepository>(() =>
                Resources.Load<SpecializationRepository>(PathToSpecializationRepository));
    }
}