using UnityEngine;

namespace Engine.Model.Office.Specializations
{
    [System.Serializable]
    public sealed class Specialization : ISpecialization
    {
        public const uint MinRange = 0;
        public const uint MaxRange = 100;

        public uint Creative
        {
            get => _creative;
            set => _creative = value;
        }

        public uint Design
        {
            get => _design;
            set => _design = value;
        }

        public uint Technology
        {
            get => _technology;
            set => _technology = value;
        }

        public uint Marketing
        {
            get => _marketing;
            set => _marketing = value;
        }

        [SerializeField] private uint _creative;

        [SerializeField, Range(MinRange, MaxRange)]
        private uint _design;

        [SerializeField] private uint _technology;

        [SerializeField, Range(MinRange, MaxRange)]
        private uint _marketing;

        public Specialization(
            in uint creative = MinRange,
            in uint design = MinRange,
            in uint technology = MinRange,
            in uint marketing = MinRange)
        {
            _creative = creative;
            _design = design;
            _technology = technology;
            _marketing = marketing;
        }

        public Specialization(ISpecialization specialization) :
            this(creative: specialization.Creative,
                design: specialization.Design,
                technology: specialization.Technology,
                marketing: specialization.Marketing
            )
        {
        }

        public override string ToString()
        {
            const string message = "Design = {0}, Creative={1}, Optimization={2}, Usability={3}";
            return string.Format(message,
                Design.ToString(),
                Creative.ToString(),
                Technology.ToString(),
                Marketing.ToString());
        }


        public override bool Equals(object obj)
        {
            if (!(obj is ISpecialization specialization)) return false;

            return _design == specialization.Design &&
                   _creative == specialization.Creative &&
                   _technology == specialization.Technology &&
                   _marketing == specialization.Marketing;
        }
    }
}