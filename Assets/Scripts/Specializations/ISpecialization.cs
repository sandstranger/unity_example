namespace Engine.Model.Office.Specializations
{
    public interface ISpecialization
    {
        uint Creative { get; }
        uint Design { get; }
        uint Technology { get; }
        uint Marketing { get; }
    }
}