using UnityEngine;

namespace Engine.Model.Office.Specializations
{
    [System.Serializable]
    internal sealed class RandomizedSpecialization : ISpecialization
    {
        public uint Creative => _creative.Value;
        public uint Design => _design.Value;
        public uint Technology => _technology.Value;
        public uint Marketing => _marketing.Value;

        [SerializeField] private RandomizedSpecializationValueRange _creative;
        [SerializeField] private RandomizedSpecializationValueRange _technology;
        [SerializeField] private RandomizedSpecializationValueRange _design;
        [SerializeField] private RandomizedSpecializationValueRange _marketing;

        internal void ResetCachedValues()
        {
            _creative.ResetCachedValue();
            _design.ResetCachedValue();
            _technology.ResetCachedValue();
            _marketing.ResetCachedValue();
        }
    }
}