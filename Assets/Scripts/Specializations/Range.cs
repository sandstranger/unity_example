using UnityEngine;

namespace Engine.Model.Office.Specializations
{
    [System.Serializable]
    internal struct Range
    {
        public int MinRange => _minRange;
        public int MaxRange => _maxRange;

        [SerializeField] private int _minRange;

        [SerializeField] private int _maxRange;
    }
}