using System;
using UniRx;

namespace Engine.Core.Service
{
    public interface IGameStateService : IDisposable
    {
        IObservable<GameState> OnGameStateChangedNotification { get; }
        bool GameStarted { get; }
        GameState CurrentGameState { get; }
    }
}