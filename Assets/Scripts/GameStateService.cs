using System;
using UniRx;

namespace Engine.Core.Service
{
    public sealed class GameStateService : IGameStateService
    {
        public IObservable<GameState> OnGameStateChangedNotification => _currentGameState;
        public bool GameStarted => _currentGameState.Value == GameState.Paused || _currentGameState.Value == GameState.Started;
        public GameState CurrentGameState => _currentGameState.Value;

        private readonly ReactiveProperty<GameState> _currentGameState = new ReactiveProperty<GameState>(GameState.Undefined);
        private GameState _previousGameState = GameState.Undefined;

        public GameStateService(IGameListener gameListener)
        {
            gameListener
                .OnGameStartedNotification
                .Subscribe(unit =>
                {
                    _currentGameState.Value = GameState.Started;
                });

            gameListener
                .OnGameOverNotification
                .Subscribe(unit =>
                {
                    _currentGameState.Value = GameState.GameOver;
                });

            gameListener
                .OnGameBackgroundNotification
                .Subscribe(onBackground =>
                {
                    if (onBackground)
                    {
                        _previousGameState = _currentGameState.Value;
                        _currentGameState.Value = GameState.Background;
                    }
                    else
                    {
                        _currentGameState.Value = _previousGameState;
                    }

                });

            gameListener
                .OnGamePaused
                .Subscribe(paused =>
                {
                    if (!GameStarted)
                    {
                        return;
                    }

                    _currentGameState.Value = paused ? GameState.Paused : GameState.Started;

                });
        }

        public void Dispose()
        {
            _currentGameState.Dispose();
        }
    }

    public enum GameState
    {
        Undefined,
        Started,
        Paused,
        Background,
        GameOver
    }
}