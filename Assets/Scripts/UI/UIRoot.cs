﻿using System;
using Engine.UI.View;
using UnityEngine;

namespace Engine.UI
{
    internal sealed class UIRoot : MonoBehaviour
    {
        internal static Func<Transform> GetUIRoot { get; private set; }

        [SerializeField] private Transform _cachedTransform;

        internal void LoadUI()
        {
            GetUIRoot = () => _cachedTransform;

            NavigationViewController.PushView<MainView>().Show();
        }

        private void OnDestroy()
        {
            GetUIRoot = null;
            NavigationViewController.DestroyAllControllers();
        }
    }
}