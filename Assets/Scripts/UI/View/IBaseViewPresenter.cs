using System;

namespace Engine.UI.View
{
    public interface IBaseViewPresenter : IDisposable
    {
        void OnViewPaused();
        void OnViewFocused();
        void OnViewShow();
        void OnViewHide();
        
        void CloseView(bool closeViewAnimated = true, bool popViewsAbove = false);
    }  
}