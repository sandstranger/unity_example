﻿using System.IO;
using System.Text;
using Engine.UI.View.HireEmployeeViews;

namespace Engine.UI.View
{
    internal static partial class NavigationViewController
    {
        private static readonly string RootPathToViews = Path.Combine("Prefabs", "UI");

        private static string GetViewPath<T>() where T : IBaseView <IBaseViewPresenter>
        {
            var targetType = typeof(T);

            var pathToView = string.Empty;

            if (targetType == typeof(InputView.InputView))
            {
                pathToView = BuildFullPathToView("InputView");
            }
            else if (targetType == typeof(GameView))
            {
                pathToView = BuildFullPathToView("GameView");
            }
            else if (targetType == typeof(HireView))
            {
                pathToView = BuildFullPathToView("HireView");
            }
            else if (targetType == typeof(FireView))
            {
                pathToView = BuildFullPathToView("FireView");
            }

            else if(targetType == typeof(SelectTypeContractView))
            {
                pathToView = BuildFullPathToView("SelectTypeContractView");
            }
            else if(targetType == typeof(SelectContractView))
            {
                pathToView = BuildFullPathToView("SelectContractView");
            }
            else if(targetType == typeof(ExecutableContractView))
            {
                pathToView = BuildFullPathToView("ExecutableContractView");
            }

            return pathToView;
        }

        private static string BuildFullPathToView(string controllerName)
        {
            return Path.Combine(RootPathToViews, controllerName, controllerName);
        }
    }
}