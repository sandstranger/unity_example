﻿using System;
using System.Collections.Generic;
using Engine.Utils.Extensions;

namespace Engine.UI.View
{
    internal sealed class ViewsList : List<ViewData>
    {
        internal bool Exists<T>() where T : class, IBaseView<IBaseViewPresenter> => !GetView<T>().Null();
        internal bool Exists(Type type) => !GetViewData(type).View.Null();

        internal ViewData GetViewData<T>() where T : class, IBaseView<IBaseViewPresenter> =>
            Find(viewData => viewData.ViewType == typeof(T));

        internal ViewData GetViewData(Type viewType) => Find(viewData => viewData.ViewType == viewType);

        internal T GetView<T>() where T : class, IBaseView<IBaseViewPresenter> => GetViewData<T>().View as T;
    }

    internal struct ViewData
    {
        internal Type ViewType => Type.GetTypeFromHandle(_runtimeViewType);
        internal IBaseView<IBaseViewPresenter> View { get; }

        private readonly RuntimeTypeHandle _runtimeViewType;

        public ViewData(Type controllerType, IBaseView<IBaseViewPresenter> view)
        {
            _runtimeViewType = controllerType.TypeHandle;
            View = view;
        }
    }
}