﻿using System;
using Engine.UI.View.InputView;
using Engine.Utils.Extensions;
using JetBrains.Annotations;
using uTools;
using UnityEngine;

namespace Engine.UI.View
{
    [RequireComponent(typeof(CanvasGroup))]
    public abstract class BaseView<T> : MonoBehaviour , IBaseView<T> where T : IBaseViewPresenter
    {
        public Transform CachedTransform => _cachedTransform;

        private bool _init = false;
        private CanvasGroup _canvasGroup;
        [SerializeField] private float _tweenDuration = 0.1f;
        private TweenAlpha _tweenAlpha;

        public T Presenter { get; private protected set; }

        private bool _destroyAnimated = false;
        [SerializeField,NotNull]
        private Transform _cachedTransform;

        #region MonobehaviorMethods

        private void Awake()
        {
            PreInit();
        }

        private void OnEnable()
        {
            OnViewEnable();
        }

        private void OnDisable()
        {
            OnViewDisable();
        }

        private void OnApplicationPause(bool pauseStatus)
        {
            if (pauseStatus)
            {
                OnViewPaused();
            }
            else
            {
                OnViewFocused();
            }
        }

        public void Destroy(bool destroyAnimated = true)
        {
            if (destroyAnimated && _canvasGroup.alpha > 0 && gameObject.activeSelf)
            {
                _destroyAnimated = destroyAnimated;
                Hide();
                return;
            }

            GameObject.Destroy(gameObject);
        }

        #endregion


        #region ControllerMethods

        public void PreInit()
        {
            if (_init) return;

            _init = true;

            _canvasGroup = GetComponent<CanvasGroup>();
            _canvasGroup.alpha = 0;

            _tweenAlpha = gameObject.AddComponent<TweenAlpha>();
            _tweenAlpha.enabled = false;
            _tweenAlpha.duration = _tweenDuration;

            _tweenAlpha.@from = 0;
            _tweenAlpha.to = 1;

            Init();
        }

        private protected virtual void Init()
        {
        }

        public virtual void OnViewEnable()
        {
        }

        public virtual void OnViewDisable()
        {
        }

        public void OnViewPaused()
        {
            Presenter?.OnViewPaused();
        }

        public void OnViewFocused()
        {
            Presenter?.OnViewFocused();
        }

        public virtual void OnViewShow()
        {
            Presenter?.OnViewShow();
        }

        public virtual void OnViewHide()
        {
            Presenter?.OnViewHide();
        }

        public virtual void Show()
        {
            PreInit();

            gameObject.SetActive(true);

            _tweenAlpha.tweenFactor = 0;
            _tweenAlpha.PlayForward();
            OnViewShow();
        }

        public virtual void Hide()
        {
            if (_tweenAlpha.Null()) return;

            _tweenAlpha.tweenFactor = 1;
            _tweenAlpha.AddOnFinished(() =>
            {
                _tweenAlpha.RemoveAllEventsOnFinished();

                if (!_destroyAnimated)
                {
                    OnViewHide();
                    gameObject.SetActive(false);
                }
                else
                {
                    GameObject.Destroy(gameObject);
                }
            });

            _tweenAlpha.PlayReverse();
        }

        public virtual void Dispose()
        {
            Presenter?.Dispose();
        }

        #endregion
    } 
}