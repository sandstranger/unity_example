﻿using System;

namespace Engine.UI.View
{
    public abstract class BaseViewPresenter<TView> : IBaseViewPresenter where TView : IBaseView<IBaseViewPresenter>
    {
        internal TView View => _view;

        private readonly TView _view;

        internal BaseViewPresenter(TView view)
        {
            this._view = view;
        }

        public virtual void OnViewPaused()
        {
        }

        public virtual void OnViewFocused()
        {
        }

        public virtual void OnViewShow()
        {
        }

        public virtual void OnViewHide()
        {
        }

        public virtual void Dispose()
        {
        }

        public virtual void CloseView(bool closeViewAnimated = true, bool popViewsAbove = false)
        {
            NavigationViewController.PopView(View.GetType(), closeViewAnimated, popViewsAbove);
        }
    }
}