﻿using System;
using System.Linq;
using UnityEngine;

namespace Engine.UI.View
{
    internal static partial class NavigationViewController
    {
        private static readonly Lazy<Transform> UiRoot = new Lazy<Transform>(() => UIRoot.GetUIRoot());

        private static readonly ViewsList ActiveViews =
            new ViewsList();

        internal static Type TopViewType => !ActiveViews.Any()
            ? null
            : ActiveViews.Last().ViewType;

        internal static T PushView<T>() where T : MonoBehaviour, IBaseView<IBaseViewPresenter>
        {
            if (ViewExists<T>()) return null;

            var view = LoadViewToScene<T>();

            view.PreInit();

            ActiveViews.Add(new ViewData(typeof(T), view));

            return (T) view;
        }

        internal static T GetView<T>() where T : MonoBehaviour, IBaseView<IBaseViewPresenter> =>
            ActiveViews.GetView<T>();

        internal static void PopView<T>(bool popViewAnimated = true, bool popViewsAbove = false)
            where T : MonoBehaviour, IBaseView<IBaseViewPresenter> =>
            PopView(typeof(T), popViewAnimated, popViewsAbove);

        internal static void PopView(Type targetViewType, bool popViewAnimated = true, bool popViewsAbove = false)
        {
            var destroyViews = false;

            if (!ViewExists(targetViewType))
            {
                return;
            }

            if (!popViewsAbove)
            {
                DestroyView(targetViewType, popViewAnimated);
                SetLastViewAsActive();
                return;
            }

            for (var i = 0; i < ActiveViews.Count; i++)
            {
                var viewData = ActiveViews[i];

                if (viewData.ViewType != targetViewType && !destroyViews) continue;

                DestroyView(viewData.ViewType, destroyAnimated: false);

                destroyViews = true;
                
            }

            SetLastViewAsActive();
        }


        private static void SetLastViewAsActive()
        {
            if (ActiveViews.Any())
            {
                ActiveViews.Last().View.OnViewShow();
            }
        }

        internal static void SetViewToTop<T>(T view) where T : MonoBehaviour, IBaseView<IBaseViewPresenter>
        {
            view.CachedTransform.SetAsLastSibling();
        }

        internal static void PopTopView(bool popViewAnimated = true)
        {
            if (!ActiveViews.Any()) return;

            DestroyView(ActiveViews.Last().ViewType, popViewAnimated);

            SetLastViewAsActive();
        }

        internal static void PopAllViews()
        {
            for (var i = 0; i < ActiveViews.Count; i++)
            {
                DestroyView(ActiveViews[i].ViewType);
            }
        }

        private static void DestroyView(Type controllerType, bool destroyAnimated = true)
        {
            var viewData = ActiveViews.GetViewData(controllerType);

            ActiveViews.Remove(viewData);

            viewData.View.OnViewHide();

            viewData.View.Dispose();

            viewData.View.Destroy(destroyAnimated);
        }

        internal static bool ViewExists<T>() where T : MonoBehaviour, IBaseView<IBaseViewPresenter> =>
            ActiveViews.Exists<T>();

        internal static bool ViewExists(Type type) => ActiveViews.Exists(type);

        private static T LoadViewToScene<T>() where T : MonoBehaviour, IBaseView<IBaseViewPresenter>
        {
            var viewPrefab = Resources.Load<T>(GetViewPath<T>());

            if (!viewPrefab)
            {
                const string message = "view {0} not exists!";

                throw new NullReferenceException(string.Format(message, typeof(T).ToString()));
            }

            var view = GameObject.Instantiate(viewPrefab.gameObject).GetComponent<T>();

            view.CachedTransform.SetParent(UiRoot.Value, false);
            view.CachedTransform.localScale = Vector3.one;

            return view;
        }
    }
}