using System;
using UnityEngine;

namespace Engine.UI.View
{
    public interface IBaseView<out T> : IDisposable
    {
        Transform CachedTransform { get; }

        T Presenter { get; }
        
        void PreInit();

        void OnViewEnable();

        void OnViewDisable();

        void OnViewPaused();

        void OnViewFocused();

        void OnViewShow();

        void OnViewHide();

        void Show();

        void Hide();

        void Destroy(bool destroyAnimated = true);
    }
}